package com.stefanstegic.CryptocurrencyRecommender.drools;

import com.stega.cryptorecommender.dto.nomics.MetadataDTO;
import com.stega.cryptorecommender.dto.nomics.TickerDTO;
import com.stega.cryptorecommender.model.CryptoSocialMedia;
import com.stega.cryptorecommender.model.Cryptocurrency;
import com.stega.cryptorecommender.util.CryptoJsonUtil;
import com.stega.cryptorecommender.util.KieUtil;
import org.junit.Assert;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

import java.io.FileNotFoundException;

public class RecommendationTests {

    @Test
    public void shouldPassActiveFilter() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        FactHandle handle = kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertNotNull(handle);

        kieSession.dispose();
    }

    @Test
    public void shouldNotPassActiveFilter() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        t1.status = "inactive";
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        FactHandle handle = kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertNotNull(handle);

        kieSession.dispose();
    }

    @Test
    public void shouldPassFilterParams() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        FactHandle handle = kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertNotNull(handle);

        kieSession.dispose();
    }

    @Test
    public void shouldNotPassFilterParams() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        t1.market_cap = -10.0;
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        FactHandle handle = kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertNotNull(handle);

        kieSession.dispose();
    }

    @Test
    public void shouldHaveSocialMedia() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        for (String key : m1.socialMedia.keySet()) {
            kieSession.insert(new CryptoSocialMedia(m1.original_symbol, key, m1.socialMedia.get(key)));
        }
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertFalse(crypto.hasSocialMedia);

        kieSession.dispose();
    }

    @Test
    public void shouldNotHaveSocialMedia() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertFalse(crypto.hasSocialMedia);

        kieSession.dispose();
    }

    @Test
    public void shouldBeAScamCoin() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        m1.socialMedia.clear();
        m1.whitepaper_url = "";
        m1.website_url = "";
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertTrue(crypto.isScamCoin);

        kieSession.dispose();
    }

    @Test
    public void shouldNotBeAScamCoin() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertFalse(crypto.isScamCoin);

        kieSession.dispose();
    }

    @Test
    public void shouldBeDailyDepreciating() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        t1.daily_delta.price_change = -10.0;
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertTrue(crypto.isDailyDepreciating);

        kieSession.dispose();
    }

    @Test
    public void shouldNotBeDailyDepreciating() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        t1.daily_delta.price_change = 10.0;
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertFalse(crypto.isDailyDepreciating);

        kieSession.dispose();
    }

    @Test
    public void shouldBeWeeklyDepreciating() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        t1.weekly_delta.price_change = -10.0;
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertTrue(crypto.isWeeklyDepreciating);

        kieSession.dispose();
    }

    @Test
    public void shouldNotBeWeeklyDepreciating() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        t1.weekly_delta.price_change = 10.0;
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertFalse(crypto.isWeeklyDepreciating);

        kieSession.dispose();
    }

    @Test
    public void shouldBeMonthlyAppreciating() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        t1.monthly_delta.price_change = 10.0;
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertTrue(crypto.isMonthlyAppreciating);

        kieSession.dispose();
    }

    @Test
    public void shouldNotBeMonthlyAppreciating() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        t1.weekly_delta.price_change = -10.0;
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertFalse(crypto.isMonthlyAppreciating);

        kieSession.dispose();
    }

    @Test
    public void shouldBeYearlyAppreciating() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        t1.yearly_delta.price_change = 10.0;
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertTrue(crypto.isYearlyAppreciating);

        kieSession.dispose();
    }

    @Test
    public void shouldNotBeYearlyAppreciating() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        t1.yearly_delta.price_change = -10.0;
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertFalse(crypto.isYearlyAppreciating);

        kieSession.dispose();
    }

    @Test
    public void shouldBeAlwaysDepreciating() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        t1.daily_delta.price_change = -10.0;
        t1.weekly_delta.price_change = -10.0;
        t1.monthly_delta.price_change = -10.0;
        t1.yearly_delta.price_change = -10.0;
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        FactHandle handle = kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertNotNull(handle);

        kieSession.dispose();
    }

    @Test
    public void shouldNotBeAlwaysDepreciating() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        t1.daily_delta.price_change = -10.0;
        t1.weekly_delta.price_change = -10.0;
        t1.monthly_delta.price_change = 10.0;
        t1.yearly_delta.price_change = -10.0;
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        FactHandle handle = kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertNotNull(handle);

        kieSession.dispose();
    }

    @Test
    public void shouldBeInAShortTermDip() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        t1.daily_delta.price_change = -10.0;
        t1.weekly_delta.price_change = -10.0;
        t1.monthly_delta.price_change = 10.0;
        t1.yearly_delta.price_change = 10.0;
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        FactHandle handle = kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertNotNull(handle);

        kieSession.dispose();
    }

    @Test
    public void shouldNotBeInAShortTermDip() throws FileNotFoundException {
        KieSession kieSession = KieUtil.newTestingSession();

        MetadataDTO m1 = CryptoJsonUtil.metaCryptoFromFile("./src/test/resources/valid-metadata-btc.json");
        TickerDTO t1 = CryptoJsonUtil.tickerCryptoFromFile("./src/test/resources/valid-ticker-btc.json");
        t1.daily_delta.price_change = 10.0;
        t1.weekly_delta.price_change = 10.0;
        t1.monthly_delta.price_change = 10.0;
        t1.yearly_delta.price_change = 10.0;
        Cryptocurrency crypto = new Cryptocurrency(m1, t1);
        FactHandle handle = kieSession.insert(crypto);

        kieSession.fireAllRules();
        Assert.assertNotNull(handle);

        kieSession.dispose();
    }
}
