package com.stega.cryptorecommender.dto.nomics;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.Date;

@Data
public class TickerDTO {
    public String symbol;
    public String name;
    public String logo_url;
    public String status;
    public Double price;
    public Double circulating_supply;
    public Double max_supply;
    public Double market_cap;
    public Double market_cap_dominance;
    public Integer num_exchanges;
    public Integer num_pairs;
    public Date first_trade;
    public Integer rank;

    @SerializedName("1d")
    public CryptoDeltaInfo daily_delta;

    @SerializedName("7d")
    public CryptoDeltaInfo weekly_delta;

    @SerializedName("30d")
    public CryptoDeltaInfo monthly_delta;

    @SerializedName("365d")
    public CryptoDeltaInfo yearly_delta;
}
