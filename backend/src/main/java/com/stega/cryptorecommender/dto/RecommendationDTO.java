package com.stega.cryptorecommender.dto;

import java.util.ArrayList;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RecommendationDTO {
	private int days;
	private ArrayList<CryptoCurrencyRecommendationDTO> cryptocurrencies = new ArrayList<CryptoCurrencyRecommendationDTO>();
	
}
