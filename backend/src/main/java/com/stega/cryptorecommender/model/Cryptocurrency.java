package com.stega.cryptorecommender.model;

import com.stega.cryptorecommender.dto.nomics.MetadataDTO;
import com.stega.cryptorecommender.dto.nomics.TickerDTO;
import org.kie.api.definition.type.PropertyReactive;

@PropertyReactive
public class Cryptocurrency {
    public MetadataDTO metaData;
    public TickerDTO ticker;
    public int score = 0;

    public Cryptocurrency(MetadataDTO metaDataDTO, TickerDTO tickerDTO) {
        metaData = metaDataDTO;
        ticker = tickerDTO;
    }

    public boolean hasSocialMedia = false;
    public boolean isScamCoin = true;
    public boolean isDailyDepreciating = false;
    public boolean isWeeklyDepreciating = false;
    public boolean isMonthlyAppreciating = false;
    public boolean isYearlyAppreciating = false;
}
